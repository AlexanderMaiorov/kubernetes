# Kubernetes

Simple .yaml for testing K8S

## Setup nginx node

### Step-1. Insalling ingress controller
```sh
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx && \
helm repo update && \
helm pull ingress-nginx/ingress-nginx && \
tar zxf *.tgz && \
rm -f *.tgz && \
cp ingress-nginx/values.yaml values.ingress-nginx.yaml && \
helm upgrade nginx-ingress ingress-nginx/ -f values.ingress-nginx.yaml
```
### Step-2. Deploying nginx-node
```sh
kubectl apply -f nginx-node.yaml
```
